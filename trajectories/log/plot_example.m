%% Load logged data
uav1 = readtable('log/uav1.csv');
uav2 = readtable('log/uav2.csv');
uav3 = readtable('log/uav3.csv');
uav4 = readtable('log/uav4.csv');

%% plot data
figure
plot3(uav1.field_point_x, uav1.field_point_y, uav1.field_point_z);
hold on
plot3(uav2.field_point_x, uav2.field_point_y, uav2.field_point_z);
plot3(uav3.field_point_x, uav3.field_point_y, uav3.field_point_z);
plot3(uav4.field_point_x, uav4.field_point_y, uav4.field_point_z);