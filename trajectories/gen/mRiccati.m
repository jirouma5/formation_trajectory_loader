function dPdt = mRiccati(P, S, Q)
P = reshape(P, size(Q)); %Convert from "n^2"-by-1 to "n"-by-"n"
dPdt = P*S*P - Q; %Determine derivative
dPdt = dPdt(:); %Convert from "n"-by-"n" to "n^2"-by-1