clc
close all
clear 

m=3; 
T=10; 
t=0:0.1:T;

b0=[1 0 0 0]';
b1=[0 1 0 0]';
b2=[0 0 1 0]';
b3=[0 0 0 1]';
B0=[kron(b0,eye(3)); zeros(1,3)];
B1=[kron(b1,eye(3)); zeros(1,3)];
B2=[kron(b2,eye(3)); zeros(1,3)];
B3=[kron(b3,eye(3)); zeros(1,3)];

D=[-1 0 0;1 -1 -1;0 1 0;0 0 1];
D_h=kron(D,eye(3));

d10=[0;-1;0];
d21=[1;-1;0];
d31=[-1;-1;0];
scale = 1.5;
d=scale*[d10;d21;d31];

mu10=1; mu21=0.7; mu31=0.5;
W1=diag([mu10,mu21,mu31]);
W2=diag([0,mu21,0]);
W3=diag([0,0,mu31]);

w10=1; w21=1; w31=1;
W1T=diag([w10,w21,w31]);
W2T=diag([0,w21,0]);
W3T=diag([0,0,w31]);

W1_h=kron(W1,eye(3));
W2_h=kron(W2,eye(3));
W3_h=kron(W3,eye(3));

W1T_h=kron(W1T,eye(3));
W2T_h=kron(W2T,eye(3));
W3T_h=kron(W3T,eye(3));

Q1=[D_h*W1_h*D_h'   -D_h*W1_h*d; -(D_h*W1_h*d)' d'*W1_h*d];
Q2=[D_h*W2_h*D_h'   -D_h*W2_h*d; -(D_h*W2_h*d)' d'*W2_h*d];
Q3=[D_h*W3_h*D_h'   -D_h*W3_h*d; -(D_h*W3_h*d)' d'*W3_h*d];

Q1T=[D_h*W1T_h*D_h'   -D_h*W1T_h*d; -(D_h*W1T_h*d)' d'*W1T_h*d];
Q2T=[D_h*W2T_h*D_h'   -D_h*W2T_h*d; -(D_h*W2T_h*d)' d'*W2T_h*d];
Q3T=[D_h*W3T_h*D_h'   -D_h*W3T_h*d; -(D_h*W3T_h*d)' d'*W3T_h*d];

r1=1; r2=1; r3=1;

R1=kron(r1,eye(3));
R2=kron(r2,eye(3));
R3=kron(r3,eye(3));


S1=B1*inv(R1)*B1';
S2=B2*inv(R2)*B2';
S3=B3*inv(R3)*B3';


%% Numerical solution of Coupled Riccati Equations
QT= [Q1T; Q2T; Q3T];% Riccati Matrix terminal value
Q= [Q1; Q2; Q3]; 
S=[S1 S2 S3];
 % Solving m-Coupled Riccati Matrix Differential Equation Backward in time
[T1 P] = ode45(@(t,P)mRiccati(P, S, Q), [T:-0.1:0], QT(:)); 

[m1 n1] = size(P);
PP = mat2cell(P, ones(m1,1), n1);
fh_reshape = @(x)reshape(x,size(Q));
PP = cellfun(fh_reshape,PP,'UniformOutput',false);

%% initial position-velocity in n-player game

% trajScale = 1.5;
trajScale = scale;

x0_0=[trajScale*cos(0) trajScale*sin(0) t(1)]';
u0_0=[-trajScale*sin(0) trajScale*cos(0) 1]';
x0_1=trajScale*[2;-2; 0]; 
x0_2=trajScale*[4; 0; 0]; 
% x0_2=trajScale*[2; 2; 0];
x0_3=trajScale*[-2; 1; 0]; 

x0=[x0_0;x0_1; x0_2; x0_3; 1];

for i=1:length(t)

    Pp=PP{i};
    P1=Pp(1:13,:);
    P2=Pp(14:26,:);
    P3=Pp(27:39,:); 

    Psi=expm(-(S1*P1+S2*P2+S3*P3)*t(i));
    u1(:,i)=-inv(R1)*B1'*P1*Psi*x0;
    u2(:,i)=-inv(R2)*B2'*P2*Psi*x0;
    u3(:,i)=-inv(R3)*B3'*P3*Psi*x0;
 
    x(:,i)=Psi*x0;

    x0(1:3)=[trajScale*cos(t(i)) trajScale*sin(t(i)) t(i)]';
    u0(:,i)=[-trajScale*sin(t(i)) trajScale*cos(t(i))   1]';
    

end


% save('x.mat','x');
% save('u1.mat','u1');
% save('u2.mat','u2');
% save('u3.mat','u3');

%% Save trajectories

% augment trajectories with heading
% uav1 = [x(1:3,:).' zeros(length(x),1)];
% uav2 = [x(4:6,:).' zeros(length(x),1)];
% uav3 = [x(7:9,:).' zeros(length(x),1)];
% uav4 = [x(10:12,:).' zeros(length(x),1)];

N = 30;
uav1 = [repmat(x0_0.', N, 1) zeros(N, 1); x(1:3,:).' zeros(length(x),1)];
uav2 = [repmat(x0_1.', N, 1) zeros(N, 1); x(4:6,:).' zeros(length(x),1)];
uav3 = [repmat(x0_2.', N, 1) zeros(N, 1); x(7:9,:).' zeros(length(x),1)];
uav4 = [repmat(x0_3.', N, 1) zeros(N, 1); x(10:12,:).' zeros(length(x),1)];

format = '%2.10f %2.10f %2.10f %2.10f\n';

fid = fopen('uav1.txt', 'w');
q = fprintf(fid, format, uav1.');
fclose(fid);

fid = fopen('uav2.txt', 'w');
q = fprintf(fid, format, uav2.');
fclose(fid);

fid = fopen('uav3.txt', 'w');
q = fprintf(fid, format, uav3.');
fclose(fid);

fid = fopen('uav4.txt', 'w');
q = fprintf(fid, format, uav4.');
fclose(fid);